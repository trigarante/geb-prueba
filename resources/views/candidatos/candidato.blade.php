<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <title>Control Candidatos</title>
  </head>
  <body>
    <div class="container">
      <h1 class="h1">Control de Candidatos</h1>
      <div class="row">
        <div class="table-responsive">
          <table class="table table-hover">
            <thead class="thead-dark">
              <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>&nbsp;</th>
              </tr>
            </thead>
            <tbody>

           @foreach ($candidatos as $candidato)
           <tr>
             <td>{{$candidato -> id}}</td>
             <td>{{$candidato -> nombre}}</td>
             <td>{{$candidato -> email}}</td>
             <td class="text-right">
               <div>
                  <button type="button" class="btn btn-outline-secondary btn-sm" data-toggle="modal" data-target="#flipFlop">
Click Me
</button>

             </td>
           </tr>
         @endforeach
       </tbody>
     </table>
    @include('create')
     </div>
    </div>
  </div>
  </body>
</html>
