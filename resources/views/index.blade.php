<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" type="text/css" media="all" href="{{asset('/css/app.css')}}" async/>

    <title>Clientes por bolsa</title>
  </head>
  <body>
    <div id="app">
        <conexion-cliente></conexion-cliente>
    </div>
   <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
  </body>
</html>
