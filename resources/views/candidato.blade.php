<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <title>Clientes por bolsa</title>
  </head>
  <body>
    <div id="app">
        <conexion-cliente></conexion-cliente>
    </div>
   <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
   </script>
  </body>
</html>
