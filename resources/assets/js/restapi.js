import axios from 'axios'

const client = axios.create({
  baseURL: 'http://localhost:8090/servicios',
  json: true
})

  getCandidato() {
    return this.execute('get', '/getALLCandidato')
  },
  getCandidato (id) {
    return this.execute('get', `/getCandidatoById?id=`+id)
  },
  createCandidato (data) {
    return this.execute('post', '/saveCandidato', data)
  },
  updateCandidato (id, data) {
    return this.execute('put', `/updateCandidatoPersonales`, data)
