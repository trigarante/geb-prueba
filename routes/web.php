<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/Candidato', function () {
    return view('candidato');
});

Route::get('/candidatosguzzle',array('uses'=>'Candidato@index'));

Route::get('/reload',array('uses'=>'Reload@index'));
