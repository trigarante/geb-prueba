<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class Candidato extends Controller
{
    public function index()
    {
      // code...
      $client = new Client();

      $response = $client -> get('http://localhost:8090/servicios/getALLCandidato',['auth' => ['nexos', 'n3X054h0rr4']]);
      $candidatos = json_decode($response->getBody()->getContents());

      return view('candidatos.candidato',compact('candidatos'));
    }
}
